#-------------------------------------------------
#
# Project created by QtCreator 2018-05-31T00:18:28
#
#-------------------------------------------------

QT       += core gui
RC_FILE = cp.rc
QMAKE_CXXFLAGS += -std=c++1y -pedantic-errors -Wall

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CopyPaste
TEMPLATE = app


SOURCES += main.cpp\
        app.cpp

HEADERS  += app.hpp

OTHER_FILES += Texts
