#include "app.hpp"

namespace CopyPaste
{

App::App(QWidget *parent) :
    QWidget(parent)
{
    mapper = new QSignalMapper(this);
    clipboard = QApplication::clipboard();

    layout = new QHBoxLayout();
    setLayout(layout);
    setWindowIcon(QIcon("CopyPaste.ico"));
    readContent();
}

App::~App()
{

}

void App::fillClipBoard(QString text)
{
    clipboard->setText(text);
    setWindowState(Qt::WindowMinimized);
}

void App::readContent()
{
    QDirIterator* folders = new QDirIterator("./Texts");

    folders->next(); // Skip ./
    folders->next(); // Skip ../

    while (folders->hasNext())
    {
        QString folderName = folders->next();
        QVBoxLayout* l = new QVBoxLayout;
        l->addWidget(new QLabel(QString(folderName.begin()+8)));
        l->setAlignment(Qt::AlignTop);

        QWidget* column = new QWidget(this);
        column->setLayout(l);
        column->show();

        QDirIterator* texts = new QDirIterator(folderName, QStringList() << "*.txt", QDir::Files);

        while (texts->hasNext())
        {
            QFile f(texts->next());
            if (!f.open(QIODevice::ReadOnly | QIODevice::Text)) continue;
            QByteArray text = f.readAll(), hotKey = "", name = "";

            // Extracting title, hot key and text from file

            int i = 0;
            while (text[i] != '\n') name.push_back(text[i++]);
            while (text[++i] != '\n') hotKey.push_back(text[i]);
            QString data = QString(text.begin() + i + 1);

            // Creating button

            QPushButton* b = new QPushButton(this);
            b->setText(name+ " ("+hotKey+")");
            b->setMinimumWidth(150);
            b->show();
            b->setToolTip(data);
            b->setShortcut(QKeySequence(QString(hotKey)));
            l->addWidget(b);

            // Handling button pressing

            mapper->setMapping(b, data);
            connect(b, SIGNAL(clicked()), mapper, SLOT(map()));
        }

        layout->addWidget(column);

    }

    connect(mapper, SIGNAL(mapped(QString)), this, SLOT(fillClipBoard(QString)));
}

}
