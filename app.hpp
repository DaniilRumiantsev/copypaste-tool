#ifndef APP_HPP
#define APP_HPP

#include <QApplication>
#include <QClipboard>
#include <QDirIterator>
#include <QFile>
#include <QGridLayout>
#include <QKeySequence>
#include <QLabel>
#include <QPushButton>
#include <QtPlugin>
#include <QSignalMapper>
#include <QWidget>

Q_IMPORT_PLUGIN (QWindowsIntegrationPlugin)

namespace CopyPaste
{

class App : public QWidget
{
    Q_OBJECT

    QSignalMapper* mapper;
    QClipboard *clipboard;

public:
    explicit App(QWidget *parent = 0);
    QHBoxLayout* layout;
    ~App();

public slots:

    /**
     * @brief fillClipBoard Fills clipboard with selected text
     * @param text Text to be added to clipboard
     */
    void fillClipBoard(QString text);

private:

    /**
     * @brief readContent Reads texts from the project folder and puts them into array
     */
    void readContent();
};

}

#endif // APP_HPP
